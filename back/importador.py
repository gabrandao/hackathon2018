from pymongo import MongoClient
from decimal import Decimal
from bson.decimal128 import Decimal128
import datetime
import csv


mongo = MongoClient()
col = mongo.transp.limpeza


with open('limpeza-2000-2015/Campus Butantã-Tabela 1.csv', 'r') as csvfile:
    lines = csv.DictReader(csvfile, delimiter=';')

    j = 0
    for line in lines:
        print(j)
        if line['Unidade'] == '':
            break
        data = {
            'Unidade': line['Unidade'],
            'DescricaoUnidade': line['Descrição Unidade'],
            'NLiquidacao': line['N.Liquidação'],
            'NPagamento': line['N.Pagamento'],
            'NProcesso': line['N.Processo'],
            'DatadoPagto': line['Data do Pagto.'],
            'ValorPago': line['Valor Pago'],
            'ClassDespesa': line['Class.Despesa'],
            'Fornecedor': line['Fornecedor'],
            'TipoPagamento': line['Tipo Pagamento'],
        }
        tmp = data['DatadoPagto']
        data['DatadoPagto'] = tmp[:tmp.index(' ')]
        print(data['DatadoPagto'])
        parts = data['DatadoPagto'].split('/')

        for i in range(3):
            if len(parts[i]) < 2:
                parts[i] = '0' + parts[i]

        data['DatadoPagto'] = datetime.datetime.strptime(
                '/'.join(parts), '%m/%d/%y')

        numeros = ['ValorPago']
        for campo in numeros:
            data[campo] = Decimal(data[campo].replace('R$', '').strip()\
                .replace('.', '').replace(',', '.'))
            data[campo] = Decimal128(data[campo])

        print(data)
        col.insert_one(data)
        j += 1

