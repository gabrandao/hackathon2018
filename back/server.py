from flask import Flask, request
from pymongo import MongoClient
import json
from decimal import Decimal
from datetime import datetime
from bson import json_util, Decimal128
from flask_cors import CORS

app = Flask(__name__)
CORS(app)

@app.route("/limpeza")
def limpeza():
    inst = request.args.get('instituto', '')

    db = MongoClient().transp

    rows = db.limpeza.find({
        'Unidade': {'$eq': inst}
    })

    items = []

    # aggregate
    date_ag = {}

    for row in rows:
        row['dia'] = row['DatadoPagto'].day
        row['mes'] = row['DatadoPagto'].month
        row['ano'] = row['DatadoPagto'].year
        if row['ano'] < 2005:
            continue


        cur_date = row['DatadoPagto'].isoformat()[:4]
        if cur_date in date_ag:
            date_ag[cur_date]['value'] += row['ValorPago'].to_decimal()
        else:
            date_ag[cur_date] = {
                'value': row['ValorPago'].to_decimal(),
                'date': row['DatadoPagto'].year,
            }

        items.append(row)

    data = []
    for date in date_ag:
        date_ag[date]['value'] = Decimal128(date_ag[date]['value'])
        data.append({
            'date': date_ag[date]['date'],
            'value': date_ag[date]['value'],
        })

    newlist = sorted(data, key=lambda k: k['date'])

    response = app.response_class(
        response=json.dumps(newlist, default=json_util.default),
        status=200,
        mimetype='application/json'
    )

    return response


@app.route("/area")
def area():
    inst = request.args.get('instituto', '')

    db = MongoClient().transp

    rows = db.area.find_one({
        'Unidade': {'$eq': int(inst)}
    })

    response = app.response_class(
        response=json.dumps(rows, default=json_util.default),
        status=200,
        mimetype='application/json'
    )

    return response


@app.route("/top10")
def top10():
    db = MongoClient().transp

    gastos = db.limpeza.aggregate([
        {'$match': {'DatadoPagto': {'$gte': datetime.strptime('1 jan 15', "%d %b %y")}}},
        {'$group': {'_id': "$Unidade", 'total': {'$sum': "$ValorPago"}}}
    ])
    gastos_dic = {}
    for gasto in gastos:
        ix = int(gasto['_id'])
        gastos_dic[ix] = gasto['total']

    areas = db.area.find()
    areas_dic = {}
    indice = []
    for area in areas:
        ix = int(area['Unidade'])
        areas_dic[ix] = area['Area']

        print(areas_dic)
        print(areas_dic[ix])
        indice.append({
            'instituto': db.limpeza.find_one({'Unidade': str(ix)})['DescricaoUnidade'],
            'indice': (gastos_dic[ix].to_decimal() / areas_dic[ix])
        })


    print(indice)
    newlist = sorted(indice, key=lambda k: k['indice'])

    for i, item in enumerate(newlist):
        newlist[i]['indice'] = Decimal128(item['indice'])


    response = app.response_class(
        response=json.dumps(newlist, default=json_util.default),
        status=200,
        mimetype='application/json'
    )

    return response

